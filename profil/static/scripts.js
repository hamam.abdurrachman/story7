$(document).ready(function (){
    $("#switch-button").click(function(){
        $("body").toggleClass("other-theme");
        $("a").toggleClass("a-alt-version");
        $("button").toggleClass("accordion-alt-version");
    });

    var acc = document.getElementsByClassName("accordion");
    var i;

    for (i = 0; i < acc.length; i++) {
        acc[i].addEventListener("click", function() {
            this.classList.toggle("active");
            var panel = this.nextElementSibling;
            if (panel.style.maxHeight) {
            panel.style.maxHeight = null;
            } else {
            panel.style.maxHeight = panel.scrollHeight + "px";
            } 
        });
    }
});